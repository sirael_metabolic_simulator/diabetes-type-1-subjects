@echo Executing..

@set state_counter=0

@set custom_pump_controller_path=%cd%\example
@set custom_pump_controller_path=%custom_pump_controller_path:"=%

@set custom_pump_controller=%cd%\example\custom_pump_controller.py
@set custom_pump_controller=%custom_pump_controller:"=%

@for /R %%G in (*.scenario.txt) do @call :DO_EXECUTE "%%G" "%%~dpG" "%%~nG" 


@exit /B 0



:DO_EXECUTE

@set state_idx=%~3
@set "state_idx=%state_idx:.=" & rem "%"

@set Parameters_File=%2metabolism.sir
@set Parameters_File=%Parameters_File:"=%

@set plot_file=%2%3.example_control.svg
@set plot_file=%plot_file:"=%

@echo[
@echo Parameters file: %Parameters_File%
@echo State index: %state_idx%
@echo State counter: %state_counter%
@echo Plot file: %plot_file%


@pushd "%custom_pump_controller_path%"
@py "%custom_pump_controller%" "%Parameters_File%" %state_counter% %1 "%plot_file%"
@popd

@set /A state_counter=%state_counter%+1

@exit /B 0

