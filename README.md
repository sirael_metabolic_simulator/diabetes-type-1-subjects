A training dataset is used to determine parameters of a particular metabolism. If such a training dataset is smaller than the original, input dataset, then it is possible to verify correctness of the determined parameters by observing the simulated metabolism, when it reacts to previously unseen events. If the simulation provides correct output within desired error range, then it is a proof that the procedure (used to determine the parameters) is correct.

If the procedure (used to determine the parameters) is correct, then we can use the full original input dataset to determine the parameters and we should obtain valid results, which should reduce the error further because the input dataset contain as much information as possible.

There are folders named "training-percentage", where the percentage gives the size of the training dataset obtained by reducing the original, input dataset. It is your choice to choose whether you prefer parameters, whose behavior can be inspected for a price of greater error, or whether you prefer to have more patients&profiles (a consequence of reducing the error) for the of sake trusting the way the parameters were computed without a chance to check it. With the full dataset, the results should be most precise, but the correctness has to be trusted, because the parameter-identification-procedure worked OK with the reduced dataset. Note that the parameters' values may differ across the percentage due to the parameter fusion used to reduce the total number of parameters to determine. Also, the profiles are identified with ordinal numbers, which are not unique across the percentage.

In each "training-percentage", there is a subfolder that represents a single patient. In each patient folder, there are multiple profiles - parameters to configure the Siraela. For the simulation, you need to feed Siraela with insulin, carbohydrates and heartbeat. Then, it will provide simulated blood and interstitial-fluid glucose levels.

The plot and error-report figure demonstrate error of each profile from the original data, the profile was fitted to. The original data are not available here due to copyright issues. If you want to verify, you have to download and parse them separately for yourself.

The source of the data is the Loop Study (sponsored by the Jaeb Center for Health Research and funded by the Helmsley Charitable Trust), but the analyses, content and conclusions presented herein are solely the responsibility of the authors and have not been reviewed or approved by the study sponsor.

Use Siraela Terminal from https://gitlab.com/sirael_metabolic_simulator/siraela to execute the simulations. To do so,  there is a simple pump-controller example coded in Python, which needs the Sirael terminal.

Files
=====
*.sirael - this is the metabolism description file
*.sir - this the compiled .sirael file, i.e.; an intermediate representation between the actual simulation and the metabolism user-friendly description
*.svg - this file depicts the simulation including error quantification
*.scenario.txt - this is a simulation scenario
*.sign.base64 - digital signature


Warning!!!
=========

In-silico simulation is a very complicated task. There are many effects, which seem to be counter-intuitive. For example, The dawn phenomenon is a an-early morning hyperglycemia after hours without food. It can be explained, but not in a simple way. Another phenomenon is lipohypertrophy, when fatty lumps forms at the site of frequently used insulin injection site, which alter insulin absorption dynamics.

Due to phenomena like these, all the Siraela metabolic programs are digitally signed so that you can verify if they come intact from this repository. Although it does not mean that anyone else could compute better parameters, it means, for sure, that someone took the best available knowledge of Sirael to make sure that the presented parameters are as much physiologically plausible as possible with the current level of knowledge. Simply said, it is too easy to compute different set of parameters with much less relative error for the known sequence of events, but with totally implausible behavior for a different set of meals, insulin boluses, activity, etc.
