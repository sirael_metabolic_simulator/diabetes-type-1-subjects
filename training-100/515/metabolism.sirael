/**
 * disassembled Siral metabolism model for in-silico patient simulation
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injure, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, Tomas, Siraela - In-Silico Metabolic Simulator with Application for Diabetes (May 18, 2023).
 * Available at
 *	SSRN: https://ssrn.com/abstract=4452634
 *  or http://dx.doi.org/10.2139/ssrn.4452634
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 * The available dataset can be limited by its respective permissions:
 *   The source of the data is the Loop Study (sponsored by the Jaeb Center for Health Research and funded by the Helmsley Charitable Trust), 
 *   but the analyses, content and conclusions presented herein are solely the responsibility of the authors and have not been reviewed 
 *   or approved by the study sponsor.
 *
 * This file was authored on 2024-03-14 21:08:01.9186226 UTC
 */

.substances begin
	glucose
	insulin
	generic_substance_1
	generic_substance_2
	generic_substance_3
	generic_substance_4
	generic_substance_5
	generic_substance_6
	generic_substance_7
	metabolic_need
.end substances

.compartments begin
	gut
	slow_gut
	delivered_insulin
	metabolic_need
	blood
	tissue
	generic_compartment_1
	generic_compartment_2
	generic_compartment_3
	generic_compartment_4
	generic_compartment_5
	generic_compartment_6
	generic_compartment_7
.end compartments

.metabolism begin
	active transport: glucose in slow_gut -> blood	(.p=438.016 .max_rate=5147.68)
	       transport: glucose in gut -> slow_gut	(.p=116.32 .max_rate=10245.1)
	active transport: glucose in gut -> blood	(.p=215.009 .max_rate=0.0259704)
	       transport: metabolic_need in metabolic_need -> blood	(.p=19.9498 .max_rate=5723.45)
	       transport: metabolic_need in metabolic_need -> generic_compartment_6	(.p=6.05314 .max_rate=782.761)
	       transport: metabolic_need in metabolic_need -> generic_compartment_5	(.p=0.216185 .max_rate=1432.65)
	       transport: metabolic_need in metabolic_need -> generic_compartment_7	(.p=9.11676 .max_rate=10247.9)
	       transport: glucose in generic_compartment_4 -> blood	(.p=256.576 .max_rate=37952.7)
	       transport: insulin in generic_compartment_4 -> blood	(.p=1.01167 .max_rate=51840)
	       transport: glucose in tissue -> generic_compartment_4	(.p=32.7757 .max_rate=120076)
	       transport: insulin in tissue -> generic_compartment_4	(.p=8.98943 .max_rate=47075.6)
	           react: glucose -> dev_null in generic_compartment_3	(.p=375.34 .max_rate=36191.6 .ratio=16.6277 .product_fraction=0.886409)
	           react: glucose & insulin -> generic_substance_4 in tissue	(.p=178.207 .max_rate=345587 .ratio=114.404 .product_fraction=0.00134931)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_1	(.p=159.674 .max_rate=1228.21 .ratio=35.4038 .product_fraction=0.00109399)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_2	(.p=365.36 .max_rate=1853.76 .ratio=18.3647 .product_fraction=0.954129)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_7	(.p=49.1617 .max_rate=156784 .ratio=0.0590567 .product_fraction=0.999775)
	           react: glucose & insulin -> generic_substance_2 in generic_compartment_5	(.p=3.59841 .max_rate=227991 .ratio=65.9885 .product_fraction=0.900729)
	           react: glucose & insulin -> generic_substance_2 in generic_compartment_2	(.p=229.473 .max_rate=345600 .ratio=0.896153 .product_fraction=0.261408)
	           react: generic_substance_2 & generic_substance_1 -> glucose in generic_compartment_5	(.p=180.145 .max_rate=11612.8 .ratio=35.3933 .product_fraction=0.999854)
	           react: generic_substance_2 & generic_substance_1 -> generic_substance_3 in generic_compartment_2	(.p=294.119 .max_rate=0.107722 .ratio=2.78588 .product_fraction=0.733635)
	           react: generic_substance_4 & metabolic_need -> generic_substance_3 in blood	(.p=8.99043 .max_rate=1135.35 .ratio=23.0502 .product_fraction=0.101006)
	   inverse react: glucose & generic_substance_5 -> generic_substance_1 in generic_compartment_6	(.p=179.792 .max_rate=153417 .ratio=0.00135937)
	           react: generic_substance_3 -> generic_substance_4 in generic_compartment_5	(.p=51.0664 .max_rate=59.5641 .ratio=9.27638 .product_fraction=0.00501276)
	           react: generic_substance_3 & generic_substance_1 -> glucose in generic_compartment_5	(.p=32.4917 .max_rate=4802.23 .ratio=7.27166 .product_fraction=0.377043)
	           react: generic_substance_3 & metabolic_need -> glucose in generic_compartment_5	(.p=83.121 .max_rate=28971.1 .ratio=90.6291 .product_fraction=0.680219)
	           react: generic_substance_4 & generic_substance_1 -> glucose in generic_compartment_5	(.p=4.26754 .max_rate=4164.2 .ratio=20.2957 .product_fraction=1)
	           react: generic_substance_4 & metabolic_need -> glucose in generic_compartment_5	(.p=0.391021 .max_rate=2574.56 .ratio=65.6849 .product_fraction=1)
	           react: generic_substance_5 & generic_substance_1 -> glucose in generic_compartment_5	(.p=18.5948 .max_rate=5869.49 .ratio=25.1375 .product_fraction=0.995136)
	           react: generic_substance_5 & metabolic_need -> glucose in generic_compartment_5	(.p=10.5239 .max_rate=7356.12 .ratio=133.039 .product_fraction=0.405706)
	           react: generic_substance_3 & generic_substance_1 -> glucose in generic_compartment_7	(.p=0.094031 .max_rate=13040.4 .ratio=77.2044 .product_fraction=0.954271)
	           react: generic_substance_3 & metabolic_need -> glucose in generic_compartment_7	(.p=1.96023 .max_rate=76430.3 .ratio=148.598 .product_fraction=0.00164406)
	           react: generic_substance_4 & generic_substance_1 -> glucose in generic_compartment_7	(.p=0.428155 .max_rate=2202.71 .ratio=36.5225 .product_fraction=0.00210037)
	           react: generic_substance_4 & metabolic_need -> glucose in generic_compartment_7	(.p=0.183227 .max_rate=5599.29 .ratio=50.5329 .product_fraction=0.899475)
	           react: generic_substance_5 & generic_substance_1 -> glucose in generic_compartment_7	(.p=9.89655 .max_rate=940.982 .ratio=117.987 .product_fraction=0.871696)
	           react: generic_substance_5 & metabolic_need -> glucose in generic_compartment_7	(.p=20 .max_rate=3259.52 .ratio=120.835 .product_fraction=0.938486)
	 inhibited react: glucose by insulin -> generic_substance_5 in generic_compartment_1	(.p=39.6259 .max_rate=74060.5 .ratio=0.278694 .product_fraction=0.729118)
	       transport: glucose in generic_compartment_7 -> dev_null	(.p=0.181125 .max_rate=120179)
	       transport: insulin in generic_compartment_7 -> dev_null	(.p=0.312667 .max_rate=24092.9)
	       transport: generic_substance_1 in generic_compartment_7 -> dev_null	(.p=1.91868 .max_rate=8639.83)
	       transport: generic_substance_3 in generic_compartment_7 -> dev_null	(.p=37.8969 .max_rate=0.0989945)
	       transport: generic_substance_4 in generic_compartment_7 -> dev_null	(.p=1.1025 .max_rate=473.47)
	       transport: generic_substance_5 in generic_compartment_7 -> dev_null	(.p=7.72674 .max_rate=5509.41)
	       transport: metabolic_need in blood -> dev_null	(.p=26.4575 .max_rate=0.220699)
	       transport: metabolic_need in generic_compartment_6 -> dev_null	(.p=2.14536 .max_rate=18358.1)
	       transport: metabolic_need in generic_compartment_5 -> dev_null	(.p=22.3571 .max_rate=25388.9)
	       transport: metabolic_need in generic_compartment_7 -> dev_null	(.p=29.9987 .max_rate=6492.25)
	       transport: glucose in slow_gut -> dev_null	(.p=0.0314917 .max_rate=219024)
	        equalize: glucose in blood <-> tissue	(.p=0.846878 .max_rate=290409)
	        equalize: insulin in blood <-> tissue	(.p=6.10616 .max_rate=33.1358)
	        equalize: generic_substance_4 in blood <-> tissue	(.p=4.72829 .max_rate=798.425)
	        equalize: glucose in blood <-> generic_compartment_1	(.p=103.978 .max_rate=6293.9)
	        equalize: insulin in blood <-> generic_compartment_1	(.p=0.00100015 .max_rate=2381.2)
	        equalize: generic_substance_4 in blood <-> generic_compartment_1	(.p=3.77108 .max_rate=49.9178)
	        equalize: generic_substance_5 in blood <-> generic_compartment_1	(.p=0.508909 .max_rate=5838.47)
	        equalize: glucose in blood <-> generic_compartment_2	(.p=243.595 .max_rate=83747.2)
	        equalize: insulin in blood <-> generic_compartment_2	(.p=1.15946 .max_rate=18552.2)
	        equalize: generic_substance_1 in blood <-> generic_compartment_2	(.p=3.88328 .max_rate=5015.97)
	        equalize: generic_substance_4 in blood <-> generic_compartment_2	(.p=1.80991 .max_rate=8638.47)
	        equalize: generic_substance_3 in blood <-> generic_compartment_2	(.p=95.9842 .max_rate=42413.6)
	        equalize: glucose in blood <-> generic_compartment_5	(.p=105.672 .max_rate=344814)
	        equalize: insulin in blood <-> generic_compartment_5	(.p=0.162565 .max_rate=51809.9)
	        equalize: generic_substance_4 in blood <-> generic_compartment_5	(.p=2.13803 .max_rate=4670.12)
	        equalize: generic_substance_5 in blood <-> generic_compartment_5	(.p=11.5215 .max_rate=14283.8)
	        equalize: generic_substance_1 in blood <-> generic_compartment_5	(.p=0.614026 .max_rate=1414.75)
	        equalize: glucose in blood <-> generic_compartment_6	(.p=16.1731 .max_rate=11200.3)
	        equalize: insulin in blood <-> generic_compartment_6	(.p=8.08217 .max_rate=43162.5)
	        equalize: generic_substance_1 in blood <-> generic_compartment_6	(.p=0.00100873 .max_rate=396.956)
	        equalize: glucose in blood <-> generic_compartment_3	(.p=171.945 .max_rate=11975.4)
	        equalize: glucose in blood <-> generic_compartment_7	(.p=144.914 .max_rate=20774.9)
	        equalize: insulin in blood <-> generic_compartment_7	(.p=0.00100805 .max_rate=39335.1)
	        equalize: generic_substance_4 in blood <-> generic_compartment_7	(.p=1.38707 .max_rate=1397.88)
	        equalize: generic_substance_5 in blood <-> generic_compartment_7	(.p=0.634315 .max_rate=933.351)
	        equalize: generic_substance_1 in blood <-> generic_compartment_7	(.p=9.20183 .max_rate=0.00859679)
	active transport: insulin in delivered_insulin -> blood	(.p=0.00106548 .max_rate=42741.2)
.end metabolism

.signals begin
	 emit: glucose in blood as {1A03867F-2F09-4EB6-8499-AC1C21025A2C}
	 emit: glucose in tissue as {B44F2278-F592-491E-BE20-6DF6999AC2D8}
	 consume: {37AA6AC1-6984-4A06-92CC-A660110D0DC7} as glucose in gut (.conversion_factor=1.79617993405901)
	 consume: {09B16B4A-54C2-4C6A-948A-3DEF8533059B} as insulin in delivered_insulin (.conversion_factor=0.9787925525055376)
	 consume: {6DFCFD02-C48C-4CE0-BD82-2D941E767A99} as metabolic_need in metabolic_need (.conversion_factor=0.01365844216540377 .sensor=true)
.end signals

.state begin
	 init: 6.8203 -> glucose in gut
	 init: 1.30458 -> glucose in slow_gut
	 init: 1.82816 -> insulin in delivered_insulin
	 init: 0.794786 -> metabolic_need in metabolic_need
	 init: 6.519 -> glucose in blood
	 init: 0.17062 -> insulin in blood
	 init: 0.0640735 -> generic_substance_1 in blood
	 init: 0.00198701 -> generic_substance_3 in blood
	 init: 0.0666306 -> generic_substance_4 in blood
	 init: 0.212666 -> generic_substance_5 in blood
	 init: 1.73805 -> generic_substance_6 in blood
	 init: 17.4486 -> generic_substance_7 in blood
	 init: 0.0229739 -> metabolic_need in blood
	 init: 6.28426 -> glucose in tissue
	 init: 2.88714e-05 -> insulin in tissue
	 init: 0.25844 -> generic_substance_1 in tissue
	 init: 1.09038 -> generic_substance_3 in tissue
	 init: 0.425797 -> generic_substance_4 in tissue
	 init: 0.300256 -> generic_substance_5 in tissue
	 init: 21.9905 -> generic_substance_6 in tissue
	 init: 38.9758 -> generic_substance_7 in tissue
	 init: 1.61825 -> metabolic_need in tissue
	 init: 6.0563 -> glucose in generic_compartment_1
	 init: 0.00773071 -> insulin in generic_compartment_1
	 init: 0.431568 -> generic_substance_1 in generic_compartment_1
	 init: 0.801749 -> generic_substance_3 in generic_compartment_1
	 init: 0.000340775 -> generic_substance_4 in generic_compartment_1
	 init: 0.78757 -> generic_substance_5 in generic_compartment_1
	 init: 99.9725 -> generic_substance_6 in generic_compartment_1
	 init: 82.4862 -> generic_substance_7 in generic_compartment_1
	 init: 1.30642 -> metabolic_need in generic_compartment_1
	 init: 9.49177 -> glucose in generic_compartment_2
	 init: 0.636585 -> insulin in generic_compartment_2
	 init: 0.962276 -> generic_substance_1 in generic_compartment_2
	 init: 14.9133 -> generic_substance_2 in generic_compartment_2
	 init: 1.23235 -> generic_substance_3 in generic_compartment_2
	 init: 0.975241 -> generic_substance_4 in generic_compartment_2
	 init: 0.550477 -> generic_substance_5 in generic_compartment_2
	 init: 22.8484 -> generic_substance_6 in generic_compartment_2
	 init: 16.2819 -> generic_substance_7 in generic_compartment_2
	 init: 0.703576 -> metabolic_need in generic_compartment_2
	 init: 9.46828 -> glucose in generic_compartment_3
	 init: 0.230775 -> insulin in generic_compartment_3
	 init: 0.0640447 -> generic_substance_1 in generic_compartment_3
	 init: 9.59602 -> generic_substance_3 in generic_compartment_3
	 init: 0.000619141 -> generic_substance_4 in generic_compartment_3
	 init: 0.424394 -> generic_substance_5 in generic_compartment_3
	 init: 14.0951 -> generic_substance_6 in generic_compartment_3
	 init: 15.6215 -> generic_substance_7 in generic_compartment_3
	 init: 1.5472 -> metabolic_need in generic_compartment_3
	 init: 8.72348 -> glucose in generic_compartment_4
	 init: 0.276781 -> insulin in generic_compartment_4
	 init: 0.282954 -> generic_substance_1 in generic_compartment_4
	 init: 1.771 -> generic_substance_3 in generic_compartment_4
	 init: 0.38922 -> generic_substance_4 in generic_compartment_4
	 init: 0.00642173 -> generic_substance_5 in generic_compartment_4
	 init: 24.5782 -> generic_substance_6 in generic_compartment_4
	 init: 100 -> generic_substance_7 in generic_compartment_4
	 init: 1.09156 -> metabolic_need in generic_compartment_4
	 init: 6.53316 -> glucose in generic_compartment_5
	 init: 0.00242981 -> insulin in generic_compartment_5
	 init: 0.0065556 -> generic_substance_1 in generic_compartment_5
	 init: 99.3051 -> generic_substance_2 in generic_compartment_5
	 init: 1.80772 -> generic_substance_3 in generic_compartment_5
	 init: 0.274433 -> generic_substance_4 in generic_compartment_5
	 init: 0.636997 -> generic_substance_5 in generic_compartment_5
	 init: 97.3901 -> generic_substance_6 in generic_compartment_5
	 init: 8.26891 -> generic_substance_7 in generic_compartment_5
	 init: 0.72772 -> metabolic_need in generic_compartment_5
	 init: 4.70472 -> glucose in generic_compartment_6
	 init: 4.50107e-05 -> insulin in generic_compartment_6
	 init: 0.0374377 -> generic_substance_1 in generic_compartment_6
	 init: 1.37292 -> generic_substance_3 in generic_compartment_6
	 init: 0.143118 -> generic_substance_4 in generic_compartment_6
	 init: 0.966344 -> generic_substance_5 in generic_compartment_6
	 init: 29.0353 -> generic_substance_6 in generic_compartment_6
	 init: 9.2668 -> generic_substance_7 in generic_compartment_6
	 init: 1.65482 -> metabolic_need in generic_compartment_6
	 init: 5.42344 -> glucose in generic_compartment_7
	 init: 0.021863 -> insulin in generic_compartment_7
	 init: 0.442799 -> generic_substance_1 in generic_compartment_7
	 init: 1.50614e-05 -> generic_substance_3 in generic_compartment_7
	 init: 0.00252338 -> generic_substance_4 in generic_compartment_7
	 init: 0.852496 -> generic_substance_5 in generic_compartment_7
	 init: 0.394101 -> generic_substance_6 in generic_compartment_7
	 init: 17.7048 -> generic_substance_7 in generic_compartment_7
	 init: 0.816252 -> metabolic_need in generic_compartment_7
.end state

.global begin
	init: 10 -> stepping //seconds
.end global
