/**
 * disassembled Siral metabolism model for in-silico patient simulation
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * rawos@rawos.com
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injure, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, Tomas, Siraela - In-Silico Metabolic Simulator with Application for Diabetes (May 18, 2023).
 * Available at
 *	SSRN: https://ssrn.com/abstract=4452634
 *  or http://dx.doi.org/10.2139/ssrn.4452634
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 * The available dataset can be limited by its respective permissions:
 *   The source of the data is the Loop Study (sponsored by the Jaeb Center for Health Research and funded by the Helmsley Charitable Trust), 
 *   but the analyses, content and conclusions presented herein are solely the responsibility of the authors and have not been reviewed 
 *   or approved by the study sponsor.
 *
 * This file was authored on 2023-11-09 15:25:35.0341681 UTC
 */

.substances begin
	glucose
	insulin
	generic_substance_1
	generic_substance_2
	generic_substance_3
	generic_substance_4
	generic_substance_5
	generic_substance_6
	generic_substance_7
	metabolic_need
.end substances

.compartments begin
	gut
	slow_gut
	delivered_insulin
	metabolic_need
	blood
	tissue
	generic_compartment_1
	generic_compartment_2
	generic_compartment_3
	generic_compartment_4
	generic_compartment_5
	generic_compartment_6
	generic_compartment_7
.end compartments

.metabolism begin
	active transport: glucose in slow_gut -> blood	(.p=188.589 .max_rate=23.8284)
	       transport: glucose in gut -> slow_gut	(.p=170.143 .max_rate=15318.3)
	active transport: glucose in gut -> blood	(.p=123.754 .max_rate=0.0177612)
	       transport: metabolic_need in metabolic_need -> blood	(.p=7.79376 .max_rate=11414.4)
	       transport: metabolic_need in metabolic_need -> generic_compartment_6	(.p=20.8983 .max_rate=10839.4)
	       transport: metabolic_need in metabolic_need -> generic_compartment_5	(.p=0.133229 .max_rate=2747.15)
	       transport: metabolic_need in metabolic_need -> generic_compartment_7	(.p=24.1873 .max_rate=2798.55)
	       transport: glucose in generic_compartment_4 -> blood	(.p=326.469 .max_rate=1.1214)
	       transport: insulin in generic_compartment_4 -> blood	(.p=4.52677 .max_rate=39203.7)
	       transport: glucose in tissue -> generic_compartment_4	(.p=399.212 .max_rate=90290.9)
	       transport: insulin in tissue -> generic_compartment_4	(.p=4.09703 .max_rate=42983.8)
	           react: glucose -> dev_null in generic_compartment_3	(.p=399.999 .max_rate=68459.3 .ratio=39.6311 .product_fraction=0.780152)
	           react: glucose & insulin -> generic_substance_4 in tissue	(.p=47.7248 .max_rate=111955 .ratio=20.2806 .product_fraction=0.960027)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_1	(.p=303.397 .max_rate=2849.81 .ratio=12.428 .product_fraction=0.999502)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_2	(.p=38.5377 .max_rate=39562.5 .ratio=44.7107 .product_fraction=0.619078)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_7	(.p=127.225 .max_rate=76356.4 .ratio=76.1131 .product_fraction=0.561885)
	           react: glucose & insulin -> generic_substance_2 in generic_compartment_5	(.p=55.9857 .max_rate=102739 .ratio=0.324642 .product_fraction=0.291802)
	           react: glucose & insulin -> generic_substance_2 in generic_compartment_2	(.p=0.001 .max_rate=197561 .ratio=59.0958 .product_fraction=0.7779)
	           react: generic_substance_2 & generic_substance_1 -> glucose in generic_compartment_5	(.p=548.072 .max_rate=148649 .ratio=91.5289 .product_fraction=0.999001)
	           react: generic_substance_2 & generic_substance_1 -> generic_substance_3 in generic_compartment_2	(.p=271.657 .max_rate=721084 .ratio=12.336 .product_fraction=0.847321)
	           react: generic_substance_4 & metabolic_need -> generic_substance_3 in blood	(.p=7.28966 .max_rate=3005.96 .ratio=1.76373 .product_fraction=0.997372)
	   inverse react: glucose & generic_substance_5 -> generic_substance_1 in generic_compartment_6	(.p=40.4896 .max_rate=59226.2 .ratio=65.8626)
	           react: generic_substance_3 -> generic_substance_4 in generic_compartment_5	(.p=43.1255 .max_rate=2112.47 .ratio=90.3504 .product_fraction=0.001)
	           react: generic_substance_3 & generic_substance_1 -> glucose in generic_compartment_5	(.p=55.463 .max_rate=40046.7 .ratio=52.0763 .product_fraction=0.730727)
	           react: generic_substance_3 & metabolic_need -> glucose in generic_compartment_5	(.p=26.9864 .max_rate=56910.1 .ratio=51.8719 .product_fraction=0.278915)
	           react: generic_substance_4 & generic_substance_1 -> glucose in generic_compartment_5	(.p=3.11733 .max_rate=5110.02 .ratio=83.1218 .product_fraction=0.598472)
	           react: generic_substance_4 & metabolic_need -> glucose in generic_compartment_5	(.p=0.181744 .max_rate=4761.09 .ratio=73.9069 .product_fraction=0.999943)
	           react: generic_substance_5 & generic_substance_1 -> glucose in generic_compartment_5	(.p=5.97888 .max_rate=5320.89 .ratio=17.1478 .product_fraction=0.489362)
	           react: generic_substance_5 & metabolic_need -> glucose in generic_compartment_5	(.p=0.64701 .max_rate=7520.07 .ratio=37.9961 .product_fraction=0.0528414)
	           react: generic_substance_3 & generic_substance_1 -> glucose in generic_compartment_7	(.p=1.67704 .max_rate=63411.7 .ratio=28.9061 .product_fraction=0.986822)
	           react: generic_substance_3 & metabolic_need -> glucose in generic_compartment_7	(.p=52.1887 .max_rate=654.232 .ratio=10.2076 .product_fraction=0.247025)
	           react: generic_substance_4 & generic_substance_1 -> glucose in generic_compartment_7	(.p=0.845424 .max_rate=166.231 .ratio=4.03208 .product_fraction=0.923267)
	           react: generic_substance_4 & metabolic_need -> glucose in generic_compartment_7	(.p=3.49756 .max_rate=3549.07 .ratio=0.431993 .product_fraction=0.793089)
	           react: generic_substance_5 & generic_substance_1 -> glucose in generic_compartment_7	(.p=3.58569 .max_rate=4984.17 .ratio=21.0242 .product_fraction=0.815526)
	           react: generic_substance_5 & metabolic_need -> glucose in generic_compartment_7	(.p=7.39013 .max_rate=1049.05 .ratio=31.8842 .product_fraction=0.812257)
	 inhibited react: glucose by insulin -> generic_substance_5 in generic_compartment_1	(.p=314.612 .max_rate=52477.1 .ratio=0.107376 .product_fraction=0.62615)
	       transport: glucose in generic_compartment_7 -> dev_null	(.p=246.763 .max_rate=16404.7)
	       transport: insulin in generic_compartment_7 -> dev_null	(.p=8.0375 .max_rate=861.112)
	       transport: generic_substance_1 in generic_compartment_7 -> dev_null	(.p=2.36597 .max_rate=8610.21)
	       transport: generic_substance_3 in generic_compartment_7 -> dev_null	(.p=61.1978 .max_rate=23061.2)
	       transport: generic_substance_4 in generic_compartment_7 -> dev_null	(.p=1.5336 .max_rate=5353.34)
	       transport: generic_substance_5 in generic_compartment_7 -> dev_null	(.p=19.8307 .max_rate=12682)
	       transport: metabolic_need in blood -> dev_null	(.p=19.7517 .max_rate=1458.64)
	       transport: metabolic_need in generic_compartment_6 -> dev_null	(.p=9.06238 .max_rate=14022.7)
	       transport: metabolic_need in generic_compartment_5 -> dev_null	(.p=5.93666 .max_rate=25796.4)
	       transport: metabolic_need in generic_compartment_7 -> dev_null	(.p=28.5202 .max_rate=19449)
	       transport: glucose in slow_gut -> dev_null	(.p=48.0825 .max_rate=168602)
	        equalize: glucose in blood <-> tissue	(.p=25.511 .max_rate=285419)
	        equalize: insulin in blood <-> tissue	(.p=4.29301 .max_rate=32039.8)
	        equalize: generic_substance_4 in blood <-> tissue	(.p=0.269094 .max_rate=5706.91)
	        equalize: glucose in blood <-> generic_compartment_1	(.p=169.339 .max_rate=53970.8)
	        equalize: insulin in blood <-> generic_compartment_1	(.p=0.682827 .max_rate=27293.4)
	        equalize: generic_substance_4 in blood <-> generic_compartment_1	(.p=2.23286 .max_rate=5686.04)
	        equalize: generic_substance_5 in blood <-> generic_compartment_1	(.p=0.390693 .max_rate=8768.67)
	        equalize: glucose in blood <-> generic_compartment_2	(.p=141.184 .max_rate=131223)
	        equalize: insulin in blood <-> generic_compartment_2	(.p=1.86607 .max_rate=44740.8)
	        equalize: generic_substance_1 in blood <-> generic_compartment_2	(.p=3.53671 .max_rate=7410.17)
	        equalize: generic_substance_4 in blood <-> generic_compartment_2	(.p=0.669951 .max_rate=43.6889)
	        equalize: generic_substance_3 in blood <-> generic_compartment_2	(.p=4.32124 .max_rate=75433.2)
	        equalize: glucose in blood <-> generic_compartment_5	(.p=55.4269 .max_rate=162351)
	        equalize: insulin in blood <-> generic_compartment_5	(.p=3.45703 .max_rate=35435)
	        equalize: generic_substance_4 in blood <-> generic_compartment_5	(.p=3.29451 .max_rate=3993.45)
	        equalize: generic_substance_5 in blood <-> generic_compartment_5	(.p=0.852427 .max_rate=7135.1)
	        equalize: generic_substance_1 in blood <-> generic_compartment_5	(.p=0.193993 .max_rate=1346.57)
	        equalize: glucose in blood <-> generic_compartment_6	(.p=96.0174 .max_rate=68296.8)
	        equalize: insulin in blood <-> generic_compartment_6	(.p=2.39266 .max_rate=33236.1)
	        equalize: generic_substance_1 in blood <-> generic_compartment_6	(.p=0.866694 .max_rate=7600.25)
	        equalize: glucose in blood <-> generic_compartment_3	(.p=327.68 .max_rate=668.403)
	        equalize: glucose in blood <-> generic_compartment_7	(.p=288.16 .max_rate=8535.35)
	        equalize: insulin in blood <-> generic_compartment_7	(.p=8.45368 .max_rate=99.6081)
	        equalize: generic_substance_4 in blood <-> generic_compartment_7	(.p=2.40269 .max_rate=6693.68)
	        equalize: generic_substance_5 in blood <-> generic_compartment_7	(.p=0.830009 .max_rate=15838.9)
	        equalize: generic_substance_1 in blood <-> generic_compartment_7	(.p=0.697774 .max_rate=1982.15)
	active transport: insulin in delivered_insulin -> blood	(.p=6.10256 .max_rate=89134.4)
.end metabolism

.signals begin
	 emit: glucose in blood as {1A03867F-2F09-4EB6-8499-AC1C21025A2C}
	 emit: glucose in tissue as {B44F2278-F592-491E-BE20-6DF6999AC2D8}
	 consume: {37AA6AC1-6984-4A06-92CC-A660110D0DC7} as glucose in gut (.conversion_factor=0.4847149086662337)
	 consume: {09B16B4A-54C2-4C6A-948A-3DEF8533059B} as insulin in delivered_insulin (.conversion_factor=1.75492292317687)
	 consume: {6DFCFD02-C48C-4CE0-BD82-2D941E767A99} as metabolic_need in metabolic_need (.conversion_factor=0.01232469331068996 .sensor=true)
.end signals

.state begin
	 init: 0.229485 -> glucose in gut
	 init: 8.05956 -> glucose in slow_gut
	 init: 10.0616 -> insulin in delivered_insulin
	 init: 1.38532 -> metabolic_need in metabolic_need
	 init: 2.60941 -> glucose in blood
	 init: 0.0430001 -> insulin in blood
	 init: 0.027752 -> generic_substance_1 in blood
	 init: 3.58408 -> generic_substance_3 in blood
	 init: 0.21856 -> generic_substance_4 in blood
	 init: 0.803241 -> generic_substance_5 in blood
	 init: 13.2183 -> generic_substance_6 in blood
	 init: 10.5054 -> generic_substance_7 in blood
	 init: 1.05154 -> metabolic_need in blood
	 init: 4.16359 -> glucose in tissue
	 init: 0.0275176 -> insulin in tissue
	 init: 0.0432932 -> generic_substance_1 in tissue
	 init: 5.55027e-07 -> generic_substance_3 in tissue
	 init: 0.0886448 -> generic_substance_4 in tissue
	 init: 0.0102754 -> generic_substance_5 in tissue
	 init: 6.05383 -> generic_substance_6 in tissue
	 init: 29.2915 -> generic_substance_7 in tissue
	 init: 1.90215 -> metabolic_need in tissue
	 init: 6.42715 -> glucose in generic_compartment_1
	 init: 0.0233215 -> insulin in generic_compartment_1
	 init: 0.0359051 -> generic_substance_1 in generic_compartment_1
	 init: 0.96307 -> generic_substance_3 in generic_compartment_1
	 init: 0.133566 -> generic_substance_4 in generic_compartment_1
	 init: 0.313785 -> generic_substance_5 in generic_compartment_1
	 init: 10.8634 -> generic_substance_6 in generic_compartment_1
	 init: 10.3211 -> generic_substance_7 in generic_compartment_1
	 init: 0.680511 -> metabolic_need in generic_compartment_1
	 init: 5.18007 -> glucose in generic_compartment_2
	 init: 0.0179885 -> insulin in generic_compartment_2
	 init: 0.0439983 -> generic_substance_1 in generic_compartment_2
	 init: 44.1458 -> generic_substance_2 in generic_compartment_2
	 init: 0.00772195 -> generic_substance_3 in generic_compartment_2
	 init: 0.0734187 -> generic_substance_4 in generic_compartment_2
	 init: 0.0289555 -> generic_substance_5 in generic_compartment_2
	 init: 0.00377908 -> generic_substance_6 in generic_compartment_2
	 init: 11.728 -> generic_substance_7 in generic_compartment_2
	 init: 0.936834 -> metabolic_need in generic_compartment_2
	 init: 8.74195 -> glucose in generic_compartment_3
	 init: 0.190714 -> insulin in generic_compartment_3
	 init: 0.307145 -> generic_substance_1 in generic_compartment_3
	 init: 1.97142 -> generic_substance_3 in generic_compartment_3
	 init: 0.127481 -> generic_substance_4 in generic_compartment_3
	 init: 1.9311 -> generic_substance_5 in generic_compartment_3
	 init: 21.9001 -> generic_substance_6 in generic_compartment_3
	 init: 7.66028 -> generic_substance_7 in generic_compartment_3
	 init: 0.914117 -> metabolic_need in generic_compartment_3
	 init: 4.92856 -> glucose in generic_compartment_4
	 init: 0.002724 -> insulin in generic_compartment_4
	 init: 0.0194354 -> generic_substance_1 in generic_compartment_4
	 init: 1.04676 -> generic_substance_3 in generic_compartment_4
	 init: 0.043888 -> generic_substance_4 in generic_compartment_4
	 init: 0.304881 -> generic_substance_5 in generic_compartment_4
	 init: 12.1556 -> generic_substance_6 in generic_compartment_4
	 init: 0.256558 -> generic_substance_7 in generic_compartment_4
	 init: 1.9701 -> metabolic_need in generic_compartment_4
	 init: 2.54862 -> glucose in generic_compartment_5
	 init: 0.109828 -> insulin in generic_compartment_5
	 init: 0.00417117 -> generic_substance_1 in generic_compartment_5
	 init: 1.19123 -> generic_substance_2 in generic_compartment_5
	 init: 6.83445 -> generic_substance_3 in generic_compartment_5
	 init: 0.306625 -> generic_substance_4 in generic_compartment_5
	 init: 0.421209 -> generic_substance_5 in generic_compartment_5
	 init: 9.45021 -> generic_substance_6 in generic_compartment_5
	 init: 11.6642 -> generic_substance_7 in generic_compartment_5
	 init: 1.03053 -> metabolic_need in generic_compartment_5
	 init: 4.40812 -> glucose in generic_compartment_6
	 init: 0.616876 -> insulin in generic_compartment_6
	 init: 0.407481 -> generic_substance_1 in generic_compartment_6
	 init: 0.201016 -> generic_substance_3 in generic_compartment_6
	 init: 0.0973408 -> generic_substance_4 in generic_compartment_6
	 init: 1.99982 -> generic_substance_5 in generic_compartment_6
	 init: 9.66272 -> generic_substance_6 in generic_compartment_6
	 init: 0.0139129 -> generic_substance_7 in generic_compartment_6
	 init: 0.710221 -> metabolic_need in generic_compartment_6
	 init: 4.67583 -> glucose in generic_compartment_7
	 init: 0.00576389 -> insulin in generic_compartment_7
	 init: 0.167441 -> generic_substance_1 in generic_compartment_7
	 init: 1.42411 -> generic_substance_3 in generic_compartment_7
	 init: 0.995901 -> generic_substance_4 in generic_compartment_7
	 init: 0.36735 -> generic_substance_5 in generic_compartment_7
	 init: 12.9426 -> generic_substance_6 in generic_compartment_7
	 init: 15.2042 -> generic_substance_7 in generic_compartment_7
	 init: 0.836119 -> metabolic_need in generic_compartment_7
.end state

.state begin
	 init: 1.00861 -> glucose in gut
	 init: 0.26331 -> glucose in slow_gut
	 init: 5.89471 -> insulin in delivered_insulin
	 init: 0.936928 -> metabolic_need in metabolic_need
	 init: 15.0056 -> glucose in blood
	 init: 0.00335917 -> insulin in blood
	 init: 0.00593262 -> generic_substance_1 in blood
	 init: 0.00129633 -> generic_substance_3 in blood
	 init: 0.134375 -> generic_substance_4 in blood
	 init: 0.423354 -> generic_substance_5 in blood
	 init: 34.4439 -> generic_substance_6 in blood
	 init: 9.66348 -> generic_substance_7 in blood
	 init: 0.717209 -> metabolic_need in blood
	 init: 16.1844 -> glucose in tissue
	 init: 0.0082095 -> insulin in tissue
	 init: 0.00853153 -> generic_substance_1 in tissue
	 init: 6.1792 -> generic_substance_3 in tissue
	 init: 0.131208 -> generic_substance_4 in tissue
	 init: 0.0228135 -> generic_substance_5 in tissue
	 init: 7.54985 -> generic_substance_6 in tissue
	 init: 6.8774 -> generic_substance_7 in tissue
	 init: 1.00287 -> metabolic_need in tissue
	 init: 15.9062 -> glucose in generic_compartment_1
	 init: 0.00720286 -> insulin in generic_compartment_1
	 init: 0.00157351 -> generic_substance_1 in generic_compartment_1
	 init: 0.952977 -> generic_substance_3 in generic_compartment_1
	 init: 0.191482 -> generic_substance_4 in generic_compartment_1
	 init: 1.0694 -> generic_substance_5 in generic_compartment_1
	 init: 29.7524 -> generic_substance_6 in generic_compartment_1
	 init: 0.394116 -> generic_substance_7 in generic_compartment_1
	 init: 0.536099 -> metabolic_need in generic_compartment_1
	 init: 15.8231 -> glucose in generic_compartment_2
	 init: 0.287848 -> insulin in generic_compartment_2
	 init: 2.17687e-05 -> generic_substance_1 in generic_compartment_2
	 init: 10.8894 -> generic_substance_2 in generic_compartment_2
	 init: 0.38467 -> generic_substance_3 in generic_compartment_2
	 init: 3.436e-05 -> generic_substance_4 in generic_compartment_2
	 init: 0.548247 -> generic_substance_5 in generic_compartment_2
	 init: 48.8643 -> generic_substance_6 in generic_compartment_2
	 init: 1.10878 -> generic_substance_7 in generic_compartment_2
	 init: 0.00817271 -> metabolic_need in generic_compartment_2
	 init: 13.716 -> glucose in generic_compartment_3
	 init: 0.174515 -> insulin in generic_compartment_3
	 init: 1.42421e-06 -> generic_substance_1 in generic_compartment_3
	 init: 0.685936 -> generic_substance_3 in generic_compartment_3
	 init: 0.204471 -> generic_substance_4 in generic_compartment_3
	 init: 0.000276037 -> generic_substance_5 in generic_compartment_3
	 init: 0.150096 -> generic_substance_6 in generic_compartment_3
	 init: 18.11 -> generic_substance_7 in generic_compartment_3
	 init: 1.08973 -> metabolic_need in generic_compartment_3
	 init: 23.0979 -> glucose in generic_compartment_4
	 init: 0.0528043 -> insulin in generic_compartment_4
	 init: 0.00317074 -> generic_substance_1 in generic_compartment_4
	 init: 0.00144844 -> generic_substance_3 in generic_compartment_4
	 init: 0.0885962 -> generic_substance_4 in generic_compartment_4
	 init: 0.148181 -> generic_substance_5 in generic_compartment_4
	 init: 21.5481 -> generic_substance_6 in generic_compartment_4
	 init: 10.8837 -> generic_substance_7 in generic_compartment_4
	 init: 0.908944 -> metabolic_need in generic_compartment_4
	 init: 15.909 -> glucose in generic_compartment_5
	 init: 0.00648959 -> insulin in generic_compartment_5
	 init: 0.272547 -> generic_substance_1 in generic_compartment_5
	 init: 39.586 -> generic_substance_2 in generic_compartment_5
	 init: 0.499705 -> generic_substance_3 in generic_compartment_5
	 init: 0.214208 -> generic_substance_4 in generic_compartment_5
	 init: 0.351321 -> generic_substance_5 in generic_compartment_5
	 init: 8.28185 -> generic_substance_6 in generic_compartment_5
	 init: 10.8706 -> generic_substance_7 in generic_compartment_5
	 init: 0.873963 -> metabolic_need in generic_compartment_5
	 init: 8.22458 -> glucose in generic_compartment_6
	 init: 0.052898 -> insulin in generic_compartment_6
	 init: 0.244412 -> generic_substance_1 in generic_compartment_6
	 init: 0.485295 -> generic_substance_3 in generic_compartment_6
	 init: 0.0415658 -> generic_substance_4 in generic_compartment_6
	 init: 0.960678 -> generic_substance_5 in generic_compartment_6
	 init: 1.97903 -> generic_substance_6 in generic_compartment_6
	 init: 0.679408 -> generic_substance_7 in generic_compartment_6
	 init: 1.95928 -> metabolic_need in generic_compartment_6
	 init: 26.059 -> glucose in generic_compartment_7
	 init: 0.68271 -> insulin in generic_compartment_7
	 init: 0.0214676 -> generic_substance_1 in generic_compartment_7
	 init: 0.0337924 -> generic_substance_3 in generic_compartment_7
	 init: 0.138883 -> generic_substance_4 in generic_compartment_7
	 init: 0.502349 -> generic_substance_5 in generic_compartment_7
	 init: 23.6714 -> generic_substance_6 in generic_compartment_7
	 init: 8.14338 -> generic_substance_7 in generic_compartment_7
	 init: 0.534394 -> metabolic_need in generic_compartment_7
.end state

.state begin
	 init: 1.21568 -> glucose in gut
	 init: 0.438953 -> glucose in slow_gut
	 init: 2.18724 -> insulin in delivered_insulin
	 init: 0.5769 -> metabolic_need in metabolic_need
	 init: 5.81391 -> glucose in blood
	 init: 0.0910334 -> insulin in blood
	 init: 0.0125016 -> generic_substance_1 in blood
	 init: 0.530352 -> generic_substance_3 in blood
	 init: 0.165852 -> generic_substance_4 in blood
	 init: 0.24002 -> generic_substance_5 in blood
	 init: 45.684 -> generic_substance_6 in blood
	 init: 22.4979 -> generic_substance_7 in blood
	 init: 1.23951 -> metabolic_need in blood
	 init: 8.27152 -> glucose in tissue
	 init: 0.00454511 -> insulin in tissue
	 init: 4.81923e-05 -> generic_substance_1 in tissue
	 init: 2.22971 -> generic_substance_3 in tissue
	 init: 0.150657 -> generic_substance_4 in tissue
	 init: 0.361911 -> generic_substance_5 in tissue
	 init: 42.1597 -> generic_substance_6 in tissue
	 init: 0.208374 -> generic_substance_7 in tissue
	 init: 0.914584 -> metabolic_need in tissue
	 init: 6.82795 -> glucose in generic_compartment_1
	 init: 0.0363766 -> insulin in generic_compartment_1
	 init: 0.00270298 -> generic_substance_1 in generic_compartment_1
	 init: 0.527316 -> generic_substance_3 in generic_compartment_1
	 init: 0.165997 -> generic_substance_4 in generic_compartment_1
	 init: 0.0117275 -> generic_substance_5 in generic_compartment_1
	 init: 21.3453 -> generic_substance_6 in generic_compartment_1
	 init: 99.9181 -> generic_substance_7 in generic_compartment_1
	 init: 0.637413 -> metabolic_need in generic_compartment_1
	 init: 7.79649 -> glucose in generic_compartment_2
	 init: 0.036221 -> insulin in generic_compartment_2
	 init: 0.0480872 -> generic_substance_1 in generic_compartment_2
	 init: 83.1192 -> generic_substance_2 in generic_compartment_2
	 init: 0.0407224 -> generic_substance_3 in generic_compartment_2
	 init: 0.51041 -> generic_substance_4 in generic_compartment_2
	 init: 1.70096 -> generic_substance_5 in generic_compartment_2
	 init: 17.8023 -> generic_substance_6 in generic_compartment_2
	 init: 0.704941 -> generic_substance_7 in generic_compartment_2
	 init: 1.38983 -> metabolic_need in generic_compartment_2
	 init: 7.31252 -> glucose in generic_compartment_3
	 init: 0.0176121 -> insulin in generic_compartment_3
	 init: 0.00102893 -> generic_substance_1 in generic_compartment_3
	 init: 0.0926763 -> generic_substance_3 in generic_compartment_3
	 init: 0.134145 -> generic_substance_4 in generic_compartment_3
	 init: 0.428817 -> generic_substance_5 in generic_compartment_3
	 init: 0.219826 -> generic_substance_6 in generic_compartment_3
	 init: 4.71461 -> generic_substance_7 in generic_compartment_3
	 init: 0.591051 -> metabolic_need in generic_compartment_3
	 init: 9.69579 -> glucose in generic_compartment_4
	 init: 0.00458928 -> insulin in generic_compartment_4
	 init: 0.10559 -> generic_substance_1 in generic_compartment_4
	 init: 0.518996 -> generic_substance_3 in generic_compartment_4
	 init: 0.215951 -> generic_substance_4 in generic_compartment_4
	 init: 0.735638 -> generic_substance_5 in generic_compartment_4
	 init: 18.7633 -> generic_substance_6 in generic_compartment_4
	 init: 2.16007 -> generic_substance_7 in generic_compartment_4
	 init: 0.362728 -> metabolic_need in generic_compartment_4
	 init: 9.19439 -> glucose in generic_compartment_5
	 init: 0.020907 -> insulin in generic_compartment_5
	 init: 0.0471784 -> generic_substance_1 in generic_compartment_5
	 init: 99.2416 -> generic_substance_2 in generic_compartment_5
	 init: 2.23548 -> generic_substance_3 in generic_compartment_5
	 init: 0.166877 -> generic_substance_4 in generic_compartment_5
	 init: 1.65849 -> generic_substance_5 in generic_compartment_5
	 init: 10.6377 -> generic_substance_6 in generic_compartment_5
	 init: 10.7609 -> generic_substance_7 in generic_compartment_5
	 init: 0.814352 -> metabolic_need in generic_compartment_5
	 init: 6.97677 -> glucose in generic_compartment_6
	 init: 0.0602234 -> insulin in generic_compartment_6
	 init: 0.028666 -> generic_substance_1 in generic_compartment_6
	 init: 0.111656 -> generic_substance_3 in generic_compartment_6
	 init: 0.998172 -> generic_substance_4 in generic_compartment_6
	 init: 0.305897 -> generic_substance_5 in generic_compartment_6
	 init: 8.03903 -> generic_substance_6 in generic_compartment_6
	 init: 20.6118 -> generic_substance_7 in generic_compartment_6
	 init: 1.25073 -> metabolic_need in generic_compartment_6
	 init: 7.79468 -> glucose in generic_compartment_7
	 init: 0.0753335 -> insulin in generic_compartment_7
	 init: 0.0354653 -> generic_substance_1 in generic_compartment_7
	 init: 4.10509 -> generic_substance_3 in generic_compartment_7
	 init: 0.0792495 -> generic_substance_4 in generic_compartment_7
	 init: 0.285649 -> generic_substance_5 in generic_compartment_7
	 init: 9.47923 -> generic_substance_6 in generic_compartment_7
	 init: 3.57675 -> generic_substance_7 in generic_compartment_7
	 init: 0.34911 -> metabolic_need in generic_compartment_7
.end state

.state begin
	 init: 6.79014 -> glucose in gut
	 init: 5.18638 -> glucose in slow_gut
	 init: 1.1953 -> insulin in delivered_insulin
	 init: 0.457994 -> metabolic_need in metabolic_need
	 init: 8.14437 -> glucose in blood
	 init: 0.0280879 -> insulin in blood
	 init: 0.123353 -> generic_substance_1 in blood
	 init: 2.41176 -> generic_substance_3 in blood
	 init: 0.0890929 -> generic_substance_4 in blood
	 init: 6.80688e-05 -> generic_substance_5 in blood
	 init: 15.2982 -> generic_substance_6 in blood
	 init: 13.3643 -> generic_substance_7 in blood
	 init: 1.05433 -> metabolic_need in blood
	 init: 8.31559 -> glucose in tissue
	 init: 0.0161433 -> insulin in tissue
	 init: 0.00262608 -> generic_substance_1 in tissue
	 init: 7.0432 -> generic_substance_3 in tissue
	 init: 0.846364 -> generic_substance_4 in tissue
	 init: 0.0862476 -> generic_substance_5 in tissue
	 init: 25.2903 -> generic_substance_6 in tissue
	 init: 10.2047 -> generic_substance_7 in tissue
	 init: 1.08591 -> metabolic_need in tissue
	 init: 13.402 -> glucose in generic_compartment_1
	 init: 0.00323275 -> insulin in generic_compartment_1
	 init: 0.025736 -> generic_substance_1 in generic_compartment_1
	 init: 5.31414e-05 -> generic_substance_3 in generic_compartment_1
	 init: 0.0558795 -> generic_substance_4 in generic_compartment_1
	 init: 0.361663 -> generic_substance_5 in generic_compartment_1
	 init: 13.8664 -> generic_substance_6 in generic_compartment_1
	 init: 23.943 -> generic_substance_7 in generic_compartment_1
	 init: 1.21948 -> metabolic_need in generic_compartment_1
	 init: 10.1405 -> glucose in generic_compartment_2
	 init: 0.0059293 -> insulin in generic_compartment_2
	 init: 0.35002 -> generic_substance_1 in generic_compartment_2
	 init: 10.5731 -> generic_substance_2 in generic_compartment_2
	 init: 1.5013 -> generic_substance_3 in generic_compartment_2
	 init: 0.000436334 -> generic_substance_4 in generic_compartment_2
	 init: 0.833254 -> generic_substance_5 in generic_compartment_2
	 init: 3.66892 -> generic_substance_6 in generic_compartment_2
	 init: 5.12313 -> generic_substance_7 in generic_compartment_2
	 init: 0.60576 -> metabolic_need in generic_compartment_2
	 init: 7.09974 -> glucose in generic_compartment_3
	 init: 0.240212 -> generic_substance_1 in generic_compartment_3
	 init: 2.2637 -> generic_substance_3 in generic_compartment_3
	 init: 0.136582 -> generic_substance_4 in generic_compartment_3
	 init: 0.36899 -> generic_substance_5 in generic_compartment_3
	 init: 44.6644 -> generic_substance_6 in generic_compartment_3
	 init: 6.38161 -> generic_substance_7 in generic_compartment_3
	 init: 0.985715 -> metabolic_need in generic_compartment_3
	 init: 7.2705 -> glucose in generic_compartment_4
	 init: 0.0726667 -> insulin in generic_compartment_4
	 init: 0.00182483 -> generic_substance_1 in generic_compartment_4
	 init: 0.0940998 -> generic_substance_3 in generic_compartment_4
	 init: 0.0370193 -> generic_substance_4 in generic_compartment_4
	 init: 0.288857 -> generic_substance_5 in generic_compartment_4
	 init: 9.92509 -> generic_substance_6 in generic_compartment_4
	 init: 8.24403 -> generic_substance_7 in generic_compartment_4
	 init: 1.50219 -> metabolic_need in generic_compartment_4
	 init: 8.89929 -> glucose in generic_compartment_5
	 init: 0.0215944 -> insulin in generic_compartment_5
	 init: 0.0398455 -> generic_substance_1 in generic_compartment_5
	 init: 2.56095 -> generic_substance_2 in generic_compartment_5
	 init: 2.2727 -> generic_substance_3 in generic_compartment_5
	 init: 0.882684 -> generic_substance_4 in generic_compartment_5
	 init: 0.374357 -> generic_substance_5 in generic_compartment_5
	 init: 8.10783 -> generic_substance_6 in generic_compartment_5
	 init: 2.52752 -> generic_substance_7 in generic_compartment_5
	 init: 0.468704 -> metabolic_need in generic_compartment_5
	 init: 12.4594 -> glucose in generic_compartment_6
	 init: 0.23446 -> insulin in generic_compartment_6
	 init: 0.197628 -> generic_substance_1 in generic_compartment_6
	 init: 5.16587 -> generic_substance_3 in generic_compartment_6
	 init: 0.840661 -> generic_substance_4 in generic_compartment_6
	 init: 0.593912 -> generic_substance_5 in generic_compartment_6
	 init: 3.37791 -> generic_substance_6 in generic_compartment_6
	 init: 21.9364 -> generic_substance_7 in generic_compartment_6
	 init: 0.977204 -> metabolic_need in generic_compartment_6
	 init: 8.41033 -> glucose in generic_compartment_7
	 init: 0.131961 -> insulin in generic_compartment_7
	 init: 0.57898 -> generic_substance_1 in generic_compartment_7
	 init: 4.62383 -> generic_substance_3 in generic_compartment_7
	 init: 0.208285 -> generic_substance_4 in generic_compartment_7
	 init: 1.70684 -> generic_substance_5 in generic_compartment_7
	 init: 7.84829 -> generic_substance_6 in generic_compartment_7
	 init: 9.35081 -> generic_substance_7 in generic_compartment_7
	 init: 0.996581 -> metabolic_need in generic_compartment_7
.end state

.state begin
	 init: 0.0783906 -> glucose in gut
	 init: 0.00595655 -> glucose in slow_gut
	 init: 0.330363 -> insulin in delivered_insulin
	 init: 0.287652 -> metabolic_need in metabolic_need
	 init: 11.0493 -> glucose in blood
	 init: 0.927788 -> insulin in blood
	 init: 2.39883e-07 -> generic_substance_1 in blood
	 init: 1.35378 -> generic_substance_3 in blood
	 init: 0.122942 -> generic_substance_4 in blood
	 init: 0.531624 -> generic_substance_5 in blood
	 init: 0.757153 -> generic_substance_6 in blood
	 init: 4.92217 -> generic_substance_7 in blood
	 init: 1.54 -> metabolic_need in blood
	 init: 10.4513 -> glucose in tissue
	 init: 0.0112384 -> insulin in tissue
	 init: 0.00656166 -> generic_substance_1 in tissue
	 init: 0.297745 -> generic_substance_3 in tissue
	 init: 0.163035 -> generic_substance_4 in tissue
	 init: 0.298157 -> generic_substance_5 in tissue
	 init: 8.3175 -> generic_substance_6 in tissue
	 init: 13.8341 -> generic_substance_7 in tissue
	 init: 0.703815 -> metabolic_need in tissue
	 init: 9.29106 -> glucose in generic_compartment_1
	 init: 0.330765 -> insulin in generic_compartment_1
	 init: 0.0423265 -> generic_substance_1 in generic_compartment_1
	 init: 0.669895 -> generic_substance_3 in generic_compartment_1
	 init: 0.282302 -> generic_substance_4 in generic_compartment_1
	 init: 1.99919 -> generic_substance_5 in generic_compartment_1
	 init: 18.3418 -> generic_substance_6 in generic_compartment_1
	 init: 3.94642 -> generic_substance_7 in generic_compartment_1
	 init: 0.803187 -> metabolic_need in generic_compartment_1
	 init: 10.8405 -> glucose in generic_compartment_2
	 init: 0.0010871 -> insulin in generic_compartment_2
	 init: 0.614482 -> generic_substance_1 in generic_compartment_2
	 init: 14.3964 -> generic_substance_2 in generic_compartment_2
	 init: 0.0528833 -> generic_substance_3 in generic_compartment_2
	 init: 0.449843 -> generic_substance_4 in generic_compartment_2
	 init: 0.28205 -> generic_substance_5 in generic_compartment_2
	 init: 5.60561 -> generic_substance_6 in generic_compartment_2
	 init: 3.69634 -> generic_substance_7 in generic_compartment_2
	 init: 0.0294118 -> metabolic_need in generic_compartment_2
	 init: 10.544 -> glucose in generic_compartment_3
	 init: 0.10071 -> insulin in generic_compartment_3
	 init: 0.00513827 -> generic_substance_1 in generic_compartment_3
	 init: 1.45543 -> generic_substance_3 in generic_compartment_3
	 init: 0.103379 -> generic_substance_4 in generic_compartment_3
	 init: 0.315487 -> generic_substance_5 in generic_compartment_3
	 init: 3.36841 -> generic_substance_6 in generic_compartment_3
	 init: 48.7063 -> generic_substance_7 in generic_compartment_3
	 init: 1.01271 -> metabolic_need in generic_compartment_3
	 init: 9.26221 -> glucose in generic_compartment_4
	 init: 0.945257 -> insulin in generic_compartment_4
	 init: 0.0238695 -> generic_substance_1 in generic_compartment_4
	 init: 1.07925 -> generic_substance_3 in generic_compartment_4
	 init: 0.042742 -> generic_substance_4 in generic_compartment_4
	 init: 0.0080322 -> generic_substance_5 in generic_compartment_4
	 init: 0.209549 -> generic_substance_6 in generic_compartment_4
	 init: 12.8206 -> generic_substance_7 in generic_compartment_4
	 init: 0.578348 -> metabolic_need in generic_compartment_4
	 init: 15.8704 -> glucose in generic_compartment_5
	 init: 0.0458436 -> insulin in generic_compartment_5
	 init: 0.0161949 -> generic_substance_1 in generic_compartment_5
	 init: 38.8212 -> generic_substance_2 in generic_compartment_5
	 init: 0.267704 -> generic_substance_3 in generic_compartment_5
	 init: 0.999079 -> generic_substance_4 in generic_compartment_5
	 init: 0.314524 -> generic_substance_5 in generic_compartment_5
	 init: 1.13357 -> generic_substance_6 in generic_compartment_5
	 init: 0.04362 -> generic_substance_7 in generic_compartment_5
	 init: 1.17932 -> metabolic_need in generic_compartment_5
	 init: 13.5148 -> glucose in generic_compartment_6
	 init: 0.600503 -> insulin in generic_compartment_6
	 init: 0.129358 -> generic_substance_1 in generic_compartment_6
	 init: 0.523324 -> generic_substance_3 in generic_compartment_6
	 init: 0.087632 -> generic_substance_4 in generic_compartment_6
	 init: 0.321606 -> generic_substance_5 in generic_compartment_6
	 init: 48.2853 -> generic_substance_6 in generic_compartment_6
	 init: 1.40898 -> generic_substance_7 in generic_compartment_6
	 init: 0.995209 -> metabolic_need in generic_compartment_6
	 init: 11.1178 -> glucose in generic_compartment_7
	 init: 0.569803 -> insulin in generic_compartment_7
	 init: 0.00919117 -> generic_substance_1 in generic_compartment_7
	 init: 0.0585412 -> generic_substance_3 in generic_compartment_7
	 init: 0.12097 -> generic_substance_4 in generic_compartment_7
	 init: 1.88213 -> generic_substance_5 in generic_compartment_7
	 init: 20.1614 -> generic_substance_6 in generic_compartment_7
	 init: 0.00338734 -> generic_substance_7 in generic_compartment_7
	 init: 0.750028 -> metabolic_need in generic_compartment_7
.end state

.global begin
	init: 10 -> stepping //seconds
.end global
