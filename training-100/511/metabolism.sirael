/**
 * disassembled Siral metabolism model for in-silico patient simulation
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injure, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, Tomas, Siraela - In-Silico Metabolic Simulator with Application for Diabetes (May 18, 2023).
 * Available at
 *	SSRN: https://ssrn.com/abstract=4452634
 *  or http://dx.doi.org/10.2139/ssrn.4452634
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 * The available dataset can be limited by its respective permissions:
 *   The source of the data is the Loop Study (sponsored by the Jaeb Center for Health Research and funded by the Helmsley Charitable Trust), 
 *   but the analyses, content and conclusions presented herein are solely the responsibility of the authors and have not been reviewed 
 *   or approved by the study sponsor.
 *
 * This file was authored on 2024-03-14 21:08:01.7942444 UTC
 */

.substances begin
	glucose
	insulin
	generic_substance_1
	generic_substance_2
	generic_substance_3
	generic_substance_4
	generic_substance_5
	generic_substance_6
	generic_substance_7
	metabolic_need
.end substances

.compartments begin
	gut
	slow_gut
	delivered_insulin
	metabolic_need
	blood
	tissue
	generic_compartment_1
	generic_compartment_2
	generic_compartment_3
	generic_compartment_4
	generic_compartment_5
	generic_compartment_6
	generic_compartment_7
.end compartments

.metabolism begin
	active transport: glucose in slow_gut -> blood	(.p=222.039 .max_rate=39.1304)
	       transport: glucose in gut -> slow_gut	(.p=78.481 .max_rate=8450.92)
	active transport: glucose in gut -> blood	(.p=0.00442398 .max_rate=104.416)
	       transport: metabolic_need in metabolic_need -> blood	(.p=15.5382 .max_rate=6785.49)
	       transport: metabolic_need in metabolic_need -> generic_compartment_6	(.p=13.9943 .max_rate=27.0497)
	       transport: metabolic_need in metabolic_need -> generic_compartment_5	(.p=26.6878 .max_rate=989.743)
	       transport: metabolic_need in metabolic_need -> generic_compartment_7	(.p=0.1256 .max_rate=3586.62)
	       transport: glucose in generic_compartment_4 -> blood	(.p=243.8 .max_rate=21353.1)
	       transport: insulin in generic_compartment_4 -> blood	(.p=1.96698 .max_rate=21458.6)
	       transport: glucose in tissue -> generic_compartment_4	(.p=218.448 .max_rate=140910)
	       transport: insulin in tissue -> generic_compartment_4	(.p=0.30766 .max_rate=23349.2)
	           react: glucose -> dev_null in generic_compartment_3	(.p=398.413 .max_rate=6642.76 .ratio=66.9966 .product_fraction=0.446909)
	           react: glucose & insulin -> generic_substance_4 in tissue	(.p=45.3594 .max_rate=216449 .ratio=97.4633 .product_fraction=0.462242)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_1	(.p=101.309 .max_rate=4044.07 .ratio=33.0127 .product_fraction=0.902338)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_2	(.p=119.982 .max_rate=128197 .ratio=77.5601 .product_fraction=0.696228)
	           react: glucose & insulin -> generic_substance_4 in generic_compartment_7	(.p=255.26 .max_rate=344882 .ratio=113.772 .product_fraction=0.851587)
	           react: glucose & insulin -> generic_substance_2 in generic_compartment_5	(.p=40.569 .max_rate=218957 .ratio=6.31909 .product_fraction=0.944252)
	           react: glucose & insulin -> generic_substance_2 in generic_compartment_2	(.p=169.729 .max_rate=345387 .ratio=88.5546 .product_fraction=0.810178)
	           react: generic_substance_2 & generic_substance_1 -> glucose in generic_compartment_5	(.p=391.429 .max_rate=253067 .ratio=86.4295 .product_fraction=0.532707)
	           react: generic_substance_2 & generic_substance_1 -> generic_substance_3 in generic_compartment_2	(.p=3.42141 .max_rate=257515 .ratio=131.836 .product_fraction=0.926679)
	           react: generic_substance_4 & metabolic_need -> generic_substance_3 in blood	(.p=5.53905 .max_rate=6401.61 .ratio=88.2412 .product_fraction=0.0535121)
	   inverse react: glucose & generic_substance_5 -> generic_substance_1 in generic_compartment_6	(.p=145.272 .max_rate=199094 .ratio=54.2659)
	           react: generic_substance_3 -> generic_substance_4 in generic_compartment_5	(.p=99.3265 .max_rate=797.513 .ratio=70.8806 .product_fraction=0.99891)
	           react: generic_substance_3 & generic_substance_1 -> glucose in generic_compartment_5	(.p=100 .max_rate=28302.8 .ratio=88.7386 .product_fraction=0.319818)
	           react: generic_substance_3 & metabolic_need -> glucose in generic_compartment_5	(.p=10.2481 .max_rate=30437.6 .ratio=10.9336 .product_fraction=0.769653)
	           react: generic_substance_4 & generic_substance_1 -> glucose in generic_compartment_5	(.p=0.00989278 .max_rate=437.817 .ratio=105.761 .product_fraction=1)
	           react: generic_substance_4 & metabolic_need -> glucose in generic_compartment_5	(.p=0.880369 .max_rate=7039.85 .ratio=149.978 .product_fraction=0.00101714)
	           react: generic_substance_5 & generic_substance_1 -> glucose in generic_compartment_5	(.p=8.95586 .max_rate=15503.5 .ratio=146.038 .product_fraction=0.709858)
	           react: generic_substance_5 & metabolic_need -> glucose in generic_compartment_5	(.p=1.92044 .max_rate=17187.6 .ratio=0.00100133 .product_fraction=0.999846)
	           react: generic_substance_3 & generic_substance_1 -> glucose in generic_compartment_7	(.p=99.9696 .max_rate=66596.5 .ratio=103.689 .product_fraction=0.978338)
	           react: generic_substance_3 & metabolic_need -> glucose in generic_compartment_7	(.p=1.73455 .max_rate=51131.6 .ratio=67.8948 .product_fraction=0.373786)
	           react: generic_substance_4 & generic_substance_1 -> glucose in generic_compartment_7	(.p=8.32555 .max_rate=3.84568 .ratio=24.7947 .product_fraction=0.822762)
	           react: generic_substance_4 & metabolic_need -> glucose in generic_compartment_7	(.p=3.37306 .max_rate=1341.74 .ratio=63.7955 .product_fraction=0.999981)
	           react: generic_substance_5 & generic_substance_1 -> glucose in generic_compartment_7	(.p=9.1294 .max_rate=17241.4 .ratio=4.07181 .product_fraction=0.572329)
	           react: generic_substance_5 & metabolic_need -> glucose in generic_compartment_7	(.p=9.87155 .max_rate=6160.11 .ratio=146.584 .product_fraction=0.184428)
	 inhibited react: glucose by insulin -> generic_substance_5 in generic_compartment_1	(.p=220.36 .max_rate=112448 .ratio=0.00100026 .product_fraction=0.955655)
	       transport: glucose in generic_compartment_7 -> dev_null	(.p=349.292 .max_rate=7572.55)
	       transport: insulin in generic_compartment_7 -> dev_null	(.p=1.73795 .max_rate=35790.3)
	       transport: generic_substance_1 in generic_compartment_7 -> dev_null	(.p=0.629272 .max_rate=2255.62)
	       transport: generic_substance_3 in generic_compartment_7 -> dev_null	(.p=49.6919 .max_rate=364.829)
	       transport: generic_substance_4 in generic_compartment_7 -> dev_null	(.p=4.22358 .max_rate=5383.02)
	       transport: generic_substance_5 in generic_compartment_7 -> dev_null	(.p=2.35359 .max_rate=13869.4)
	       transport: metabolic_need in blood -> dev_null	(.p=0.407468 .max_rate=1509.31)
	       transport: metabolic_need in generic_compartment_6 -> dev_null	(.p=2.62126 .max_rate=8.28042)
	       transport: metabolic_need in generic_compartment_5 -> dev_null	(.p=29.9698 .max_rate=2165.77)
	       transport: metabolic_need in generic_compartment_7 -> dev_null	(.p=1.83452 .max_rate=18621.8)
	       transport: glucose in slow_gut -> dev_null	(.p=11.2332 .max_rate=172958)
	        equalize: glucose in blood <-> tissue	(.p=5.0249 .max_rate=331369)
	        equalize: insulin in blood <-> tissue	(.p=4.9805 .max_rate=25732.7)
	        equalize: generic_substance_4 in blood <-> tissue	(.p=2.03456 .max_rate=127.299)
	        equalize: glucose in blood <-> generic_compartment_1	(.p=182.576 .max_rate=61760.2)
	        equalize: insulin in blood <-> generic_compartment_1	(.p=3.33057 .max_rate=27093)
	        equalize: generic_substance_4 in blood <-> generic_compartment_1	(.p=0.759806 .max_rate=6928.08)
	        equalize: generic_substance_5 in blood <-> generic_compartment_1	(.p=1.11365 .max_rate=17034.7)
	        equalize: glucose in blood <-> generic_compartment_2	(.p=229.676 .max_rate=59553.3)
	        equalize: insulin in blood <-> generic_compartment_2	(.p=5.54396 .max_rate=40918.5)
	        equalize: generic_substance_1 in blood <-> generic_compartment_2	(.p=4.91524 .max_rate=4416.07)
	        equalize: generic_substance_4 in blood <-> generic_compartment_2	(.p=0.661327 .max_rate=8596.18)
	        equalize: generic_substance_3 in blood <-> generic_compartment_2	(.p=29.0394 .max_rate=82405.9)
	        equalize: glucose in blood <-> generic_compartment_5	(.p=247.941 .max_rate=64446.6)
	        equalize: insulin in blood <-> generic_compartment_5	(.p=0.476992 .max_rate=38175)
	        equalize: generic_substance_4 in blood <-> generic_compartment_5	(.p=8.30188 .max_rate=1048.52)
	        equalize: generic_substance_5 in blood <-> generic_compartment_5	(.p=1.3346 .max_rate=10139.8)
	        equalize: generic_substance_1 in blood <-> generic_compartment_5	(.p=0.001 .max_rate=467.185)
	        equalize: glucose in blood <-> generic_compartment_6	(.p=303.757 .max_rate=80786.1)
	        equalize: insulin in blood <-> generic_compartment_6	(.p=1.70561 .max_rate=50202.9)
	        equalize: generic_substance_1 in blood <-> generic_compartment_6	(.p=0.00142735 .max_rate=8450.09)
	        equalize: glucose in blood <-> generic_compartment_3	(.p=320.543 .max_rate=38648.6)
	        equalize: glucose in blood <-> generic_compartment_7	(.p=26.9413 .max_rate=216220)
	        equalize: insulin in blood <-> generic_compartment_7	(.p=0.854913 .max_rate=32837.6)
	        equalize: generic_substance_4 in blood <-> generic_compartment_7	(.p=0.951225 .max_rate=4669.68)
	        equalize: generic_substance_5 in blood <-> generic_compartment_7	(.p=6.96624 .max_rate=17279.5)
	        equalize: generic_substance_1 in blood <-> generic_compartment_7	(.p=4.3643 .max_rate=5304.83)
	active transport: insulin in delivered_insulin -> blood	(.p=0.0858653 .max_rate=49463.1)
.end metabolism

.signals begin
	 emit: glucose in blood as {1A03867F-2F09-4EB6-8499-AC1C21025A2C}
	 emit: glucose in tissue as {B44F2278-F592-491E-BE20-6DF6999AC2D8}
	 consume: {37AA6AC1-6984-4A06-92CC-A660110D0DC7} as glucose in gut (.conversion_factor=4.414329146031514)
	 consume: {09B16B4A-54C2-4C6A-948A-3DEF8533059B} as insulin in delivered_insulin (.conversion_factor=0.9541618572483529)
	 consume: {6DFCFD02-C48C-4CE0-BD82-2D941E767A99} as metabolic_need in metabolic_need (.conversion_factor=0.01250851015494357 .sensor=true)
.end signals

.state begin
	 init: 0.151027 -> glucose in gut
	 init: 17.3509 -> glucose in slow_gut
	 init: 0.645877 -> insulin in delivered_insulin
	 init: 0.635253 -> metabolic_need in metabolic_need
	 init: 8.08584 -> glucose in blood
	 init: 0.182027 -> insulin in blood
	 init: 0.011938 -> generic_substance_1 in blood
	 init: 1.58715 -> generic_substance_3 in blood
	 init: 0.130057 -> generic_substance_4 in blood
	 init: 0.88933 -> generic_substance_5 in blood
	 init: 36.3239 -> generic_substance_6 in blood
	 init: 6.27099 -> generic_substance_7 in blood
	 init: 0.228341 -> metabolic_need in blood
	 init: 9.31643 -> glucose in tissue
	 init: 0.124783 -> insulin in tissue
	 init: 0.493856 -> generic_substance_1 in tissue
	 init: 0.727154 -> generic_substance_3 in tissue
	 init: 0.166719 -> generic_substance_4 in tissue
	 init: 1.99764 -> generic_substance_5 in tissue
	 init: 0.000986951 -> generic_substance_6 in tissue
	 init: 5.8848 -> generic_substance_7 in tissue
	 init: 0.958482 -> metabolic_need in tissue
	 init: 12.1214 -> glucose in generic_compartment_1
	 init: 0.0229209 -> insulin in generic_compartment_1
	 init: 0.000233582 -> generic_substance_1 in generic_compartment_1
	 init: 0.160481 -> generic_substance_3 in generic_compartment_1
	 init: 0.0883259 -> generic_substance_4 in generic_compartment_1
	 init: 0.795512 -> generic_substance_5 in generic_compartment_1
	 init: 5.92688 -> generic_substance_6 in generic_compartment_1
	 init: 0.564798 -> generic_substance_7 in generic_compartment_1
	 init: 1.05746 -> metabolic_need in generic_compartment_1
	 init: 8.0226 -> glucose in generic_compartment_2
	 init: 1.83509e-05 -> insulin in generic_compartment_2
	 init: 0.0330347 -> generic_substance_1 in generic_compartment_2
	 init: 14.7653 -> generic_substance_2 in generic_compartment_2
	 init: 1.84367 -> generic_substance_3 in generic_compartment_2
	 init: 0.168496 -> generic_substance_4 in generic_compartment_2
	 init: 0.0308113 -> generic_substance_5 in generic_compartment_2
	 init: 0.0971697 -> generic_substance_6 in generic_compartment_2
	 init: 2.40277 -> generic_substance_7 in generic_compartment_2
	 init: 1.05822 -> metabolic_need in generic_compartment_2
	 init: 8.6111 -> glucose in generic_compartment_3
	 init: 0.151136 -> insulin in generic_compartment_3
	 init: 0.0454435 -> generic_substance_1 in generic_compartment_3
	 init: 0.377182 -> generic_substance_3 in generic_compartment_3
	 init: 0.000595343 -> generic_substance_4 in generic_compartment_3
	 init: 1.99788 -> generic_substance_5 in generic_compartment_3
	 init: 73.4146 -> generic_substance_6 in generic_compartment_3
	 init: 74.2889 -> generic_substance_7 in generic_compartment_3
	 init: 0.401515 -> metabolic_need in generic_compartment_3
	 init: 9.90274 -> glucose in generic_compartment_4
	 init: 1.87338e-07 -> insulin in generic_compartment_4
	 init: 0.26244 -> generic_substance_1 in generic_compartment_4
	 init: 4.40243 -> generic_substance_3 in generic_compartment_4
	 init: 0.244135 -> generic_substance_4 in generic_compartment_4
	 init: 1.98161 -> generic_substance_5 in generic_compartment_4
	 init: 39.7582 -> generic_substance_6 in generic_compartment_4
	 init: 18.845 -> generic_substance_7 in generic_compartment_4
	 init: 1.64562 -> metabolic_need in generic_compartment_4
	 init: 7.97444 -> glucose in generic_compartment_5
	 init: 0.0194152 -> insulin in generic_compartment_5
	 init: 0.16033 -> generic_substance_1 in generic_compartment_5
	 init: 43.5621 -> generic_substance_2 in generic_compartment_5
	 init: 0.0627972 -> generic_substance_3 in generic_compartment_5
	 init: 0.0219394 -> generic_substance_4 in generic_compartment_5
	 init: 8.53965e-06 -> generic_substance_5 in generic_compartment_5
	 init: 19.6269 -> generic_substance_6 in generic_compartment_5
	 init: 3.89016 -> generic_substance_7 in generic_compartment_5
	 init: 0.319568 -> metabolic_need in generic_compartment_5
	 init: 10.5034 -> glucose in generic_compartment_6
	 init: 0.209714 -> insulin in generic_compartment_6
	 init: 0.614902 -> generic_substance_1 in generic_compartment_6
	 init: 3.45086 -> generic_substance_3 in generic_compartment_6
	 init: 0.520477 -> generic_substance_4 in generic_compartment_6
	 init: 0.209033 -> generic_substance_5 in generic_compartment_6
	 init: 94.2524 -> generic_substance_6 in generic_compartment_6
	 init: 48.0291 -> generic_substance_7 in generic_compartment_6
	 init: 0.82077 -> metabolic_need in generic_compartment_6
	 init: 7.13155 -> glucose in generic_compartment_7
	 init: 0.090633 -> insulin in generic_compartment_7
	 init: 0.0027419 -> generic_substance_1 in generic_compartment_7
	 init: 6.40565 -> generic_substance_3 in generic_compartment_7
	 init: 0.465505 -> generic_substance_4 in generic_compartment_7
	 init: 0.0663022 -> generic_substance_5 in generic_compartment_7
	 init: 11.8375 -> generic_substance_6 in generic_compartment_7
	 init: 14.6456 -> generic_substance_7 in generic_compartment_7
	 init: 0.271359 -> metabolic_need in generic_compartment_7
.end state

.global begin
	init: 10 -> stepping //seconds
.end global
